/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:HelloWorldAdminApplication.java
 *    Date:2023/11/18 下午11:45
 *    Author:赵士杰
 */

package com.hw.admin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;


@EnableAsync
@ComponentScan("com.hw")
@MapperScan("com.hw.system.admin.mapper")
@SpringBootApplication
public class HelloWorldAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(HelloWorldAdminApplication.class, args);
    }

}
