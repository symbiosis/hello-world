/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:HelloWorldConfig.java
 *    Date:2023/11/10 下午9:17
 *    Author:赵士杰
 */

package com.hw.admin.config;

import lombok.Data;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @Project hello-world
 * @ClassName HelloWorldConfig
 * @Description 系统配置类
 * @Author Jie
 * @Date 2023/11/10 21:17
 */
@Getter
@Configuration
@ConfigurationProperties(prefix = "hello-world")
public class HelloWorldConfig {
    /**
     * 系统名称
     */
    private String name;
    /**
     * 系统版本
     */
    private String version;
    /**
     * 系统版权
     */
    private String copyrightYear;
    /**
     * 系统文件路径
     */
    private String fileStoragePath;


    public void setName(String name) {
        this.name = name;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setCopyrightYear(String copyrightYear) {
        this.copyrightYear = copyrightYear;
    }

    public void setFileStoragePath(String fileStoragePath) {
        this.fileStoragePath = fileStoragePath;
    }

    public String getImagePath() {
        return fileStoragePath+"/image";
    }

    public String getAvatarPath() {
        return fileStoragePath+"/avatar";
    }

}
