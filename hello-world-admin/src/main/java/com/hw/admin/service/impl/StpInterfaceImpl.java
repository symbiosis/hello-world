/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:StpInterfaceImpl.java
 *    Date:2023/11/26 下午7:23
 *    Author:赵士杰
 */

package com.hw.admin.service.impl;

import cn.dev33.satoken.stp.StpInterface;
import com.hw.system.admin.pojo.SysRole;
import com.hw.system.admin.service.ISysMenuService;
import com.hw.system.admin.service.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Project hello-world
 * @ClassName StpInterfaceImpl
 * @Description 自定义Sa-Token权限加载接口实现类
 * @Author 赵士杰
 * @Date 2023/11/26 11:21
 */
@Component
public class StpInterfaceImpl implements StpInterface {

    @Autowired
    private ISysMenuService sysMenuService;

    @Autowired
    private ISysRoleService sysRoleService;

    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        return sysMenuService.getUserPermissions(Long.parseLong(String.valueOf(loginId)));
    }

    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        List<SysRole> userRole = sysRoleService.getUserRole(Long.parseLong(String.valueOf(loginId)));
        return userRole.parallelStream()
                .map(SysRole::getRoleKey)
                .distinct()
                .collect(Collectors.toList());
    }

}
