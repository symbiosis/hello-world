/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:SysGroupController.java
 *    Date:2023/12/3 上午12:03
 *    Author:赵士杰
 */

package com.hw.admin.controller.system;

import com.hw.common.entity.AjaxResult;
import com.hw.system.admin.pojo.SysGroup;
import com.hw.system.admin.service.ISysGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Project hello-world
 * @ClassName SysGroupController
 * @Description 用户组接口
 * @Author 赵士杰
 * @Date 2023/11/27 21:08
 */
@RestController
@RequestMapping("/api/admin/group")
public class SysGroupController {

    @Autowired
    private ISysGroupService sysGroupService;

    @GetMapping("/list")
    public AjaxResult getGroupList() {
        return AjaxResult.success(sysGroupService.getGroupList());
    }

    @PostMapping("/add")
    public AjaxResult addGroup(@RequestBody SysGroup sysGroup) {
        if (sysGroupService.addGroup(sysGroup)) {
            return AjaxResult.success();
        }
        return AjaxResult.error();
    }

    @PutMapping("/update")
    public AjaxResult updateGroup(@RequestBody SysGroup sysGroup) {
        if (sysGroupService.updateGroupById(sysGroup)) {
            return AjaxResult.success();
        }
        return AjaxResult.error();
    }

    @GetMapping("/recycle")
    public AjaxResult findUserGroupRecycle() {
        return AjaxResult.success(sysGroupService.findUserGroupRecycle());
    }

    @DeleteMapping("delete")
    public AjaxResult deleteGroup(@RequestBody List<Integer> groupIds) {
        if (sysGroupService.batchDeleteGroupByIds(groupIds)) {
            return AjaxResult.success();
        }
        return AjaxResult.error();
    }

    @PutMapping("/recover")
    public AjaxResult recoverGroup(@RequestBody List<Integer> groupIds) {
        if (sysGroupService.batchRecoverGroupByIds(groupIds)) {
            return AjaxResult.success();
        }
        return AjaxResult.error();
    }


}
