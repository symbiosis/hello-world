/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:ServerController.java
 *    Date:2023/11/19 下午7:03
 *    Author:赵士杰
 */

package com.hw.admin.controller.monitor;

import com.hw.common.entity.AjaxResult;
import com.hw.monitor.service.IServerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Project hello-world
 * @ClassName ServerController
 * @Description 监控服务
 * @Author Jie
 * @Date 2023/11/5 22:12
 */
@Slf4j
@RestController
@RequestMapping("/api/admin/monitor/server")
public class ServerController {

    @Autowired
    private IServerService serverService;

    @GetMapping("/info/{type}")
    public AjaxResult getServerInfo(@PathVariable String type) {
        return AjaxResult.success(serverService.getServerInfo(type));
    }

}
