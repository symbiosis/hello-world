/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:SysRoleController.java
 *    Date:2023/11/26 下午7:23
 *    Author:赵士杰
 */

package com.hw.admin.controller.system;

import com.hw.system.admin.service.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Project hello-world
 * @ClassName SysRoleController
 * @Description //TODO
 * @Author 赵士杰
 * @Date 2023/11/26 12:05
 */
@RequestMapping("/api/admin/role")
@RestController
public class SysRoleController {

    @Autowired
    private ISysRoleService roleService;

}
