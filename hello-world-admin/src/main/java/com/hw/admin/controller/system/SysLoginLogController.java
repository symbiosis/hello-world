/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:SysLoginLogController.java
 *    Date:2023/11/19 下午7:03
 *    Author:赵士杰
 */

package com.hw.admin.controller.system;

import com.hw.common.entity.AjaxResult;
import com.hw.system.admin.service.ISysLoginLogService;
import com.hw.system.admin.vo.SysLoginLogVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * @Project hello-world
 * @ClassName SysLoginLogController
 * @Description 系统登录记录 前端控制器
 * @Author 赵士杰
 * @Date 2023/11/19 12:28
 */
@RestController
@RequestMapping("/api/admin/log/login")
public class SysLoginLogController {

    @Autowired
    private ISysLoginLogService sysLoginLogService;

    @GetMapping("list")
    public AjaxResult getLoginLogList() {
        List<SysLoginLogVo> list = sysLoginLogService.getLoginLogList();
        return AjaxResult.success(list);
    }

}
