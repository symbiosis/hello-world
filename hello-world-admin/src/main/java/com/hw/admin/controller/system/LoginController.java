/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:LoginController.java
 *    Date:2023/12/3 上午12:03
 *    Author:赵士杰
 */

package com.hw.admin.controller.system;

import com.hw.common.entity.AjaxResult;
import com.hw.common.utils.ip.UserAgentUtils;
import com.hw.system.admin.service.ILoginService;
import com.hw.system.admin.vo.LoginUserVo;
import com.hw.system.admin.vo.TokenInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Project hello-world
 * @ClassName UserLoginController
 * @Description //TODO
 * @Author Jie
 * @Date 2023/11/12 20:57
 */
@RestController
@RequestMapping("/api/admin")
public class LoginController {

    @Autowired
    private ILoginService loginService;

    @GetMapping("/test")
    public void testUserAgent() {
        System.out.println(UserAgentUtils.getBrowser());    //Edge
        System.out.println(UserAgentUtils.getDeviceType()); //Desktop
        System.out.println(UserAgentUtils.getPlatform());   //Win10
        System.out.println(UserAgentUtils.getBrowserType()); //Browser
        System.out.println(UserAgentUtils.getBrowserMajorVersion()); //0
    }

    @PostMapping("/login")
    public AjaxResult login(@RequestBody LoginUserVo loginUserVo) {
        //调用登录方法，返回Token
        TokenInfoVo token = loginService.login(loginUserVo);
        return AjaxResult.success(token);
    }

}
