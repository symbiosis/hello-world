/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:SysRoleMenuController.java
 *    Date:2023/11/27 下午9:06
 *    Author:赵士杰
 */

package com.hw.admin.controller.system;

import com.hw.common.entity.AjaxResult;
import com.hw.system.admin.service.ISysRoleMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Project hello-world
 * @ClassName SysRoleMenuController
 * @Description //TODO
 * @Author 赵士杰
 * @Date 2023/11/27 20:15
 */
@RequestMapping("/api/admin/roleMenu")
@RestController
public class SysRoleMenuController {

    @Autowired
    private ISysRoleMenuService sysRoleMenuService;

    @PostMapping("/add/{roleId}")
    public AjaxResult addRoleMenu(@PathVariable Long roleId, @RequestBody List<Long> menuIds) {
        boolean result = sysRoleMenuService.addRoleMenu(roleId, menuIds);
        if (result) {
            return AjaxResult.success();
        }
        return AjaxResult.error();
    }

}
