/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:SysMenuController.java
 *    Date:2023/11/27 下午9:06
 *    Author:赵士杰
 */

package com.hw.admin.controller.system;

import com.hw.common.entity.AjaxResult;
import com.hw.common.utils.security.SecurityUtils;
import com.hw.system.admin.service.ISysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Project hello-world
 * @ClassName SysMenuController
 * @Description //TODO
 * @Author 赵士杰
 * @Date 2023/11/26 9:43
 */
@RequestMapping("/api/admin/menu")
@RestController
public class SysMenuController {

    @Autowired
    private ISysMenuService sysMenuService;


    @GetMapping("/userMenuTree")
    public AjaxResult getUserMenuTree() {
        return AjaxResult.success(sysMenuService.getUserMenuTree(SecurityUtils.getLoginId()));
    }

}
