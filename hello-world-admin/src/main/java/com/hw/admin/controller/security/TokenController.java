/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:TokenController.java
 *    Date:2023/12/3 上午12:03
 *    Author:赵士杰
 */

package com.hw.admin.controller.security;

import com.hw.common.entity.AjaxResult;
import com.hw.system.admin.service.ITokenService;
import com.hw.system.admin.vo.TokenConfigVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Project hello-world
 * @ClassName TokenController
 * @Description Token配置信息
 * @Author Jie
 * @Date 2023/11/18 17:51
 */
@Slf4j
@RestController
@RequestMapping("/api/admin/security")
public class TokenController {

    @Autowired
    private ITokenService tokenService;

    @GetMapping("/token/info")
    public AjaxResult tokenInfo() {
        TokenConfigVo config = tokenService.getTokenInfo();
        return AjaxResult.success(config);
    }

    @PostMapping("/token/update")
    public AjaxResult updateToken(@RequestBody TokenConfigVo tokenConfig) {
        log.info("修改Token配置信息:{}", tokenConfig);
        tokenService.updateTokenConfig(tokenConfig);
        return AjaxResult.success();
    }

}
