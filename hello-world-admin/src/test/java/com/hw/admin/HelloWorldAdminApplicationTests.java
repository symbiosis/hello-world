/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:HelloWorldAdminApplicationTests.java
 *    Date:2023/10/29 下午8:01
 *    Author:赵士杰
 */

package com.hw.admin;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class HelloWorldAdminApplicationTests {

    @Test
    void contextLoads() {
    }

}
