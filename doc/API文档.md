# HelloWorld-Admin ^V1.0.0^ API 文档

Base URLs:

* <a href="http://localhost:8080/api/admin">开发环境: http://localhost:8080/api/admin</a>

## 接口文档

### Token

#### 获取 Token 配置信息

##### 请求格式

| Key  | Value                |
|------|----------------------|
| 请求方式 | GET                  |
| 请求路径 | /security/token/info |

##### 响应数据结构

| 名称   | 数据类型        | 必选   | 约束   | 中文名    | 说明   |
|------|-------------|------|------|--------|------|
| msg  | string      | true | none | 系统响应信息 | none |
| code | integer     | true | none | 系统响应码  | none |
| data | TokenConfig | true | none | 系统响应数据 | none |

##### 响应示例

```json
{
  "msg": "操作成功",
  "code": 200,
  "data": {
    "tokenName": "HelloWorldToken",
    "timeout": 259200,
    "activeTimeout": -1,
    "dynamicActiveTimeout": false,
    "isConcurrent": false,
    "isShare": false,
    "maxLoginCount": 5,
    "maxTryTimes": 12,
    "isReadBody": false,
    "isReadHeader": true,
    "isReadCookie": false,
    "isWriteHeader": false,
    "tokenStyle": "tik",
    "dataRefreshPeriod": 30,
    "tokenSessionCheckLogin": true,
    "autoRenew": true,
    "tokenPrefix": "hello",
    "isPrint": true,
    "isLog": false,
    "logLevel": "trace",
    "logLevelInt": 1,
    "isColorLog": false
  }
}
```

#### 更新 Token 配置信息

##### 请求格式

| Key  | Value                  |
|------|------------------------|
| 请求方式 | POST                   |
| 请求路径 | /security/token/update |

##### 请求参数

**Body**

| 名称   | 位置   | 数据类型        | 必选 | 说明        |
|------|------|-------------|----|-----------|
| body | body | TokenConfig | 是  | Token配置信息 |

##### 响应数据结构

| 名称   | 数据类型    | 必选   | 约束   | 中文名    | 说明   |
|------|---------|------|------|--------|------|
| msg  | string  | true | none | 系统响应信息 | none |
| code | integer | true | none | 系统响应码  | none |

##### 响应示例

```json
{
  "msg": "操作成功",
  "code": 200
}
```

### 服务监控

#### 获取服务信息

##### 请求格式

| Key  | Value                       |
|------|-----------------------------|
| 请求方式 | GET                         |
| 请求路径 | /monitor/server/info/{type} |

##### 请求参数

**Path**

| 名称   | 位置   | 类型     | 必选 | 说明                                 |
|------|------|--------|----|------------------------------------|
| type | path | string | 是  | 服务类型（redis、cpu、disk、jvm、memory、os） |

#### 获取 OS 信息

##### 请求格式

| Key  | Value                   |
|------|-------------------------|
| 请求方式 | GET                     |
| 请求路径 | /monitor/server/info/os |

##### 响应数据结构

| 名称   | 数据类型    | 必选   | 约束   | 中文名    | 说明   |
|------|---------|------|------|--------|------|
| msg  | string  | true | none | 系统响应信息 | none |
| code | integer | true | none | 系统响应码  | none |
| data | OS      | true | none | 系统响应数据 | none |

##### 响应示例

```json
{
  "msg": "操作成功",
  "code": 200,
  "data": {
    "computerName": "DESKTOP-0J5F84B",
    "computerIp": "192.168.1.5",
    "osName": "Windows 10",
    "osArch": "amd64"
  }
}
```

#### 获取 Redis 信息

##### 请求格式

| Key  | Value                      |
|------|----------------------------|
| 请求方式 | GET                        |
| 请求路径 | /monitor/server/info/redis |

##### 响应数据结构

| 名称   | 数据类型    | 必选   | 约束   | 中文名    | 说明   |
|------|---------|------|------|--------|------|
| msg  | string  | true | none | 系统响应信息 | none |
| code | integer | true | none | 系统响应码  | none |
| data | Redis   | true | none | 系统响应数据 | none |

##### 响应示例

```json
{
  "msg": "操作成功",
  "code": 200,
  "data": {
    "os": "Linux 3.10.0-1160.99.1.el7.x86_64 x86_64",
    "version": "7.0.13",
    "gccVersion": "12.2.0",
    "mode": "standalone",
    "port": "6379",
    "curConnectedClients": "1",
    "hisConnectedClients": "8",
    "runTime": "4小时32分钟",
    "usedMemory": "1.0 MB",
    "usedMemoryPeak": "1.1 MB",
    "usedMemoryLua": "31.0 KB",
    "usedMemoryScripts": "184 B",
    "replacePolicy": "noeviction",
    "keyTotal": "13",
    "keyMissCount": "29",
    "keyHitCount": "144",
    "expiredKeyCount": "0",
    "aofSize": "34.2 KB",
    "aofEnabled": "1",
    "aofLastWriteStatus": "ok",
    "rdbLastSaveStatus": "ok",
    "rdbSaveCount": "9",
    "rdbLastSaveTime": "2023-11-18 21:48:59",
    "rdbChangesSinceLastSave": "0",
    "totalNetOutputSize": "33.4 KB",
    "totalNetInputSize": "25.7 KB",
    "hisCommandProcessed": "220"
  }
}
```

#### 获取 CPU 信息

##### 请求格式

| Key  | Value                    |
|------|--------------------------|
| 请求方式 | GET                      |
| 请求路径 | /monitor/server/info/cpu |

##### 响应数据结构

| 名称   | 数据类型    | 必选   | 约束   | 中文名    | 说明   |
|------|---------|------|------|--------|------|
| msg  | string  | true | none | 系统响应信息 | none |
| code | integer | true | none | 系统响应码  | none |
| data | CPU     | true | none | 系统响应数据 | none |

##### 响应示例

```json
{
  "msg": "操作成功",
  "code": 200,
  "data": {
    "name": "Intel(R) Core(TM) i7-8750H CPU @ 2.20GHz",
    "physicalCoreNum": 6,
    "logicalCoreNum": 12,
    "cpuUsage": 8.32,
    "systemUsage": 3.35,
    "userUsage": 4.48,
    "idle": 91.68,
    "wait": 0
  }
}
```

#### 获取 DISK 信息

##### 请求格式

| Key  | Value                     |
|------|---------------------------|
| 请求方式 | GET                       |
| 请求路径 | /monitor/server/info/disk |

##### 响应数据结构

| 名称   | 数据类型    | 必选   | 约束   | 中文名    | 说明   |
|------|---------|------|------|--------|------|
| msg  | string  | true | none | 系统响应信息 | none |
| code | integer | true | none | 系统响应码  | none |
| data | Disk    | true | none | 系统响应数据 | none |

##### 响应示例

```json
{
  "msg": "操作成功",
  "code": 200,
  "data": [
    {
      "dirName": "C:\\",
      "sysTypeName": "NTFS",
      "typeName": "本地固定磁盘 (C:)",
      "total": "118.6 GB",
      "available": "59.7 GB",
      "used": "58.9 GB",
      "usage": 49.67
    },
    {
      "dirName": "D:\\",
      "sysTypeName": "NTFS",
      "typeName": "本地固定磁盘 (D:)",
      "total": "540.9 GB",
      "available": "502.2 GB",
      "used": "38.7 GB",
      "usage": 7.15
    },
    {
      "dirName": "E:\\",
      "sysTypeName": "NTFS",
      "typeName": "本地固定磁盘 (E:)",
      "total": "390.6 GB",
      "available": "369.9 GB",
      "used": "20.7 GB",
      "usage": 5.31
    }
  ]
}
```

#### 获取 Memory 信息

##### 请求格式

| Key  | Value                       |
|------|-----------------------------|
| 请求方式 | GET                         |
| 请求路径 | /monitor/server/info/memory |

##### 响应数据结构

| 名称   | 数据类型    | 必选   | 约束   | 中文名    | 说明   |
|------|---------|------|------|--------|------|
| msg  | string  | true | none | 系统响应信息 | none |
| code | integer | true | none | 系统响应码  | none |
| data | Memory  | true | none | 系统响应数据 | none |

##### 响应示例

```json
{
  "msg": "操作成功",
  "code": 200,
  "data": {
    "total": 31.86,
    "used": 14.87,
    "available": 16.99,
    "usage": 46.66
  }
}
```

#### 获取 JVM 信息

##### 请求格式

| Key  | Value                    |
|------|--------------------------|
| 请求方式 | GET                      |
| 请求路径 | /monitor/server/info/jvm |

##### 响应数据结构

| 名称   | 数据类型    | 必选   | 约束   | 中文名    | 说明   |
|------|---------|------|------|--------|------|
| msg  | string  | true | none | 系统响应信息 | none |
| code | integer | true | none | 系统响应码  | none |
| data | JVM     | true | none | 系统响应数据 | none |

##### 响应示例

```json
{
  "msg": "操作成功",
  "code": 200,
  "data": {
    "total": 664.5,
    "max": 7250,
    "used": 174.73,
    "available": 489.77,
    "usage": 26.3,
    "version": "1.8.0_381",
    "home": "D:\\JAVA\\jdk-8u381\\jre",
    "userDir": "D:\\Jetbrains\\IDEA\\IDEAWorkPlace\\hello-world",
    "startTime": "2023-11-18 21:53:00",
    "runTime": "0天0小时12分钟",
    "inputArgs": "[-agentlib:jdwp=transport=dt_socket,address=127.0.0.1:63991,suspend=y,server=n, -XX:TieredStopAtLevel=1, -Xverify:none, -Dspring.output.ansi.enabled=always, -Dcom.sun.management.jmxremote, -Dspring.jmx.enabled=true, -Dspring.liveBeansView.mbeanDomain, -Dspring.application.admin.enabled=true, -Dmanagement.endpoints.jmx.exposure.include=*, -javaagent:C:\\Users\\Jie\\AppData\\Local\\JetBrains\\IntelliJIdea2023.2\\captureAgent\\debugger-agent.jar=file:/C:/Users/Jie/AppData/Local/Temp/capture.props, -Dfile.encoding=UTF-8]"
  }
}
```

### 用户组

#### 列表查询

##### 请求格式

| Key  | Value       |
|------|-------------|
| 请求方式 | GET         |
| 请求路径 | /group/list |

##### 响应数据结构

| 名称   | 数据类型      | 必选   | 约束   | 中文名    | 说明   |
|------|-----------|------|------|--------|------|
| msg  | string    | true | none | 系统响应信息 | none |
| code | integer   | true | none | 系统响应码  | none |
| data | UserGroup | true | none | 系统响应数据 | none |

##### 响应示例

```json
{
  "msg": "操作成功",
  "code": 200,
  "data": [
    {
      "groupId": 2,
      "name": "系统监控组",
      "description": "系统监控功能",
      "sort": 2,
      "status": true,
      "createBy": "admin",
      "createTime": "2023-11-26 17:16:59",
      "updateBy": "张三",
      "updateTime": "2023-11-26 17:17:02"
    }
  ]
}
```

#### 回收站

##### 请求格式

| Key  | Value          |
|------|----------------|
| 请求方式 | GET            |
| 请求路径 | /group/recycle |

##### 响应数据结构

| 名称   | 数据类型      | 必选   | 约束   | 中文名    | 说明   |
|------|-----------|------|------|--------|------|
| msg  | string    | true | none | 系统响应信息 | none |
| code | integer   | true | none | 系统响应码  | none |
| data | UserGroup | true | none | 系统响应数据 | none |

##### 响应示例

```json
{
  "msg": "操作成功",
  "code": 200,
  "data": [
    {
      "groupId": 1,
      "name": "初级用户组",
      "description": "初级用户",
      "sort": 1,
      "status": true,
      "createBy": "admin",
      "createTime": "2023-11-20 19:41:47",
      "updateBy": "张三",
      "updateTime": "2023-11-20 19:41:51"
    }
  ]
}
```

#### 添加（待更新）

##### 请求格式

| Key  | Value      |
|------|------------|
| 请求方式 | POST       |
| 请求路径 | /group/add |

##### 请求参数

**Body**

| 名称          | 位置   | 类型      | 必选 | 中文名         | 说明   |
|-------------|------|---------|----|-------------|------|
| name        | body | string  | 是  | 用户组名称       | none |
| description | body | string  | 是  | 用户组描述       | none |
| sort        | body | integer | 是  | 显示顺序        | none |
| status      | body | boolean | 是  | 状态（1正常 0停用） | none |

#### 修改（待更新）

##### 请求格式

| Key  | Value         |
|------|---------------|
| 请求方式 | PUT           |
| 请求路径 | /group/update |

##### 请求参数

**Body**

| 名称          | 位置   | 类型      | 必选 | 中文名         | 说明   |
|-------------|------|---------|----|-------------|------|
| name        | body | string  | 是  | 用户组名称       | none |
| description | body | string  | 是  | 用户组描述       | none |
| sort        | body | integer | 是  | 显示顺序        | none |
| status      | body | boolean | 是  | 状态（1正常 0停用） | none |

#### 批量删除

##### 请求格式

| Key  | Value         |
|------|---------------|
| 请求方式 | DELETE        |
| 请求路径 | /group/delete |

##### 请求参数

**Body**

| 名称   | 位置   | 类型            | 必选 | 中文名       | 说明   |
|------|------|---------------|----|-----------|------|
| body | body | array[number] | 是  | 用户组 Id 列表 | none |

##### 响应数据结构

| 名称   | 数据类型    | 必选   | 约束   | 中文名    | 说明   |
|------|---------|------|------|--------|------|
| msg  | string  | true | none | 系统响应信息 | none |
| code | integer | true | none | 系统响应码  | none |

##### 响应示例

```json
{
  "msg": "操作成功",
  "code": 200
}
```

#### 批量恢复

##### 请求格式

| Key  | Value          |
|------|----------------|
| 请求方式 | PUT            |
| 请求路径 | /group/recover |

##### 请求参数

**Body**

| 名称   | 位置   | 类型            | 必选 | 中文名       | 说明   |
|------|------|---------------|----|-----------|------|
| body | body | array[number] | 是  | 用户组 Id 列表 | none |

##### 响应数据结构

| 名称   | 数据类型    | 必选   | 约束   | 中文名    | 说明   |
|------|---------|------|------|--------|------|
| msg  | string  | true | none | 系统响应信息 | none |
| code | integer | true | none | 系统响应码  | none |

##### 响应示例

```json
{
  "msg": "操作成功",
  "code": 200
}
```

## 数据模型

### CPU（待更新）

### TokenInfo【Token 信息】

```json
{
  "tokenName": "string",
  "tokenValue": "string",
  "isLogin": true,
  "loginDevice": "string"
}
```

| 名称          | 类型      | 必选   | 约束   | 中文名            | 说明   |
|-------------|---------|------|------|----------------|------|
| tokenName   | string  | true | none | token 名称       | none |
| tokenValue  | string  | true | none | token 值        | none |
| isLogin     | boolean | true | none | 此 token 是否已经登录 | none |
| loginDevice | string  | true | none | 登录设备类型         | none |

### LoginUser【用户登录】

```json
{
  "username": "string",
  "password": "string"
}
```

| 名称       | 类型     | 必选   | 约束   | 中文名 | 说明   |
|----------|--------|------|------|-----|------|
| username | string | true | none | 用户名 | none |
| password | string | true | none | 密码  | none |

### UserGroup【用户组】

```json
{
  "groupId": 0,
  "name": "string",
  "description": "string",
  "sort": 0,
  "status": true,
  "createBy": "string",
  "createTime": "string",
  "updateBy": "string",
  "updateTime": "string"
}
```

| 名称          | 类型      | 必选    | 约束   | 中文名         | 说明   |
|-------------|---------|-------|------|-------------|------|
| groupId     | integer | true  | none | 用户组id       | none |
| name        | string  | true  | none | 用户组名称       | none |
| description | string  | true  | none | 用户组描述       | none |
| sort        | integer | true  | none | 显示顺序        | none |
| status      | boolean | true  | none | 状态（1正常 0停用） | none |
| createBy    | string  | false | none | 创建者         | none |
| createTime  | string  | true  | none | 创建时间        | none |
| updateBy    | string  | false | none | 更新者         | none |
| updateTime  | string  | true  | none | 更新时间        | none |

### Redis【缓存】

```json
{
  "os": "string",
  "version": "string",
  "gccVersion": "string",
  "mode": "string",
  "port": "string",
  "curConnectedClients": "string",
  "hisConnectedClients": "string",
  "runTime": "string",
  "usedMemory": "string",
  "usedMemoryPeak": "string",
  "usedMemoryLua": "string",
  "usedMemoryScripts": "string",
  "replacePolicy": "string",
  "keyTotal": "string",
  "keyMissCount": "string",
  "keyHitCount": "string",
  "expiredKeyCount": "string",
  "aofSize": "string",
  "aofEnabled": "string",
  "aofLastWriteStatus": "string",
  "rdbLastSaveStatus": "string",
  "rdbSaveCount": "string",
  "rdbLastSaveTime": "string",
  "rdbChangesSinceLastSave": "string",
  "totalNetOutputSize": "string",
  "totalNetInputSize": "string",
  "hisCommandProcessed": "string"
}
```

| 名称                      | 类型     | 必选   | 约束   | 中文名                      | 说明   |
|-------------------------|--------|------|------|--------------------------|------|
| os                      | string | true | none | 操作系统信息                   | none |
| version                 | string | true | none | Redis版本                  | none |
| gccVersion              | string | true | none | Redis使用的GCC版本            | none |
| mode                    | string | true | none | Redis运行模式                | none |
| port                    | string | true | none | Redis实例正在侦听的TCP端口号       | none |
| curConnectedClients     | string | true | none | 当前连接的客户端数量               | none |
| hisConnectedClients     | string | true | none | 历史连接数                    | none |
| runTime                 | string | true | none | Redis运行时间                | none |
| usedMemory              | string | true | none | 内存占用                     | none |
| usedMemoryPeak          | string | true | none | Redis内存峰值                | none |
| usedMemoryLua           | string | true | none | 存储Lua脚本的内存大小             | none |
| usedMemoryScripts       | string | true | none | 用于存储脚本的内存大小              | none |
| replacePolicy           | string | true | none | 达到最大内存后的淘汰策略             | none |
| keyTotal                | string | true | none | 当前正在保存的键总数               | none |
| keyMissCount            | string | true | none | 键未命中次数                   | none |
| keyHitCount             | string | true | none | 键命中次数                    | none |
| expiredKeyCount         | string | true | none | 过期键数量                    | none |
| aofSize                 | string | true | none | AOF文件大小                  | none |
| aofEnabled              | string | true | none | 是否启用AOF持久化，1（已启用）        | none |
| aofLastWriteStatus      | string | true | none | 最后一次AOF写入的状态             | none |
| rdbLastSaveStatus       | string | true | none | 最后一次完成的RDB后台保存的状态，此处为ok。 | none |
| rdbSaveCount            | string | true | none | 执行RDB保存的次数               | none |
| rdbLastSaveTime         | string | true | none | 上次RDB保存的时间               | none |
| rdbChangesSinceLastSave | string | true | none | 上次RDB保存以来发生的修改次数，此处为0    | none |
| totalNetOutputSize      | string | true | none | 累计网络输出量                  | none |
| totalNetInputSize       | string | true | none | 累计网络输入字节数                | none |
| hisCommandProcessed     | string | true | none | 历史执行的命令数量                | none |

### TokenConfig【Token 配置】

```json
{
  "tokenName": "string",
  "timeout": 0,
  "activeTimeout": 0,
  "dynamicActiveTimeout": true,
  "isConcurrent": true,
  "isShare": true,
  "maxLoginCount": 0,
  "maxTryTimes": 0,
  "isReadBody": true,
  "isReadHeader": true,
  "isReadCookie": true,
  "isWriteHeader": true,
  "tokenStyle": "string",
  "dataRefreshPeriod": 0,
  "tokenSessionCheckLogin": true,
  "autoRenew": true,
  "tokenPrefix": "string",
  "isPrint": true,
  "isLog": true,
  "logLevel": "string",
  "logLevelInt": 0,
  "isColorLog": true
}
```

| 名称                     | 类型      | 必选   | 约束   | 中文名                      | 说明   |
|------------------------|---------|------|------|--------------------------|------|
| tokenName              | string  | true | none | Token 名称                 | none |
| timeout                | number  | true | none | Token 有效期（单位：秒）          | none |
| activeTimeout          | number  | true | none | Token 最低活跃频率（单位：秒）       | none |
| dynamicActiveTimeout   | boolean | true | none | 是否启用动态 activeTimeout 功能  | none |
| isConcurrent           | boolean | true | none | 是否允许同一账号并发登录             | none |
| isShare                | boolean | true | none | 在多人登录同一账号时，是否共用一个 token  | none |
| maxLoginCount          | integer | true | none | 同一账号最大登录数量               | none |
| maxTryTimes            | integer | true | none | 在每次创建 Token 时的最高循环次数     | none |
| isReadBody             | boolean | true | none | 是否读取请求体中的 Token          | none |
| isReadHeader           | boolean | true | none | 是否读取请求头中的 Token          | none |
| isReadCookie           | boolean | true | none | 是否读取 Cookie 中的 Token     | none |
| isWriteHeader          | boolean | true | none | 是否写入响应头中的 Token          | none |
| tokenStyle             | string  | true | none | Token 样式                 | none |
| dataRefreshPeriod      | integer | true | none | 清理过期数据间隔的时间              | none |
| tokenSessionCheckLogin | boolean | true | none | 获取 Token-Session 时是否必须登录 | none |
| autoRenew              | boolean | true | none | 是否打开自动续签                 | none |
| tokenPrefix            | string  | true | none | Token 前缀                 | none |
| isPrint                | boolean | true | none | 是否打印 Sa-Token 执行日志       | none |
| isLog                  | boolean | true | none | 是否记录 Sa-Token 日志         | none |
| logLevel               | string  | true | none | 日志级别                     | none |
| logLevelInt            | integer | true | none | 日志级别对应的整数值               | none |
| isColorLog             | boolean | true | none | 是否打印彩色日志                 | none |

