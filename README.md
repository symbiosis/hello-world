# hello-world 开源项目

hello-world开源项目


## 系统模块结构

| 模块                   | 描述         |
|----------------------|------------|
| hello-word           | 父模块，依赖版本管理 |
| hello-word-admin     | web模块      |
| hello-word-common    | 公共模块       |
| hello-word-exception | 异常模块       |
| hello-word-framework | 框架扩展模块     |
| hello-word-monitor   | 监控模块       |
| hello-word-quartz    | 定时任务模块     |
| hello-word-system    | 系统模块       |

## POM 版本依赖

| 组件                 | 版本      | 描述                     | 项目地址                                                                                             |
|--------------------|---------|------------------------|--------------------------------------------------------------------------------------------------|
| browscap           | 1.4.0   | 获取用户代理信息               | [https://github.com/blueconic/browscap-java](https://github.com/blueconic/browscap-java)         |
| sa-token           | 1.37.0  | 轻量级 Java 权限认证框架        | [https://github.com/dromara/sa-token](https://github.com/dromara/sa-token)                       |
| oshi-core          | 6.4.6   | 获取系统信息                 | [https://github.com/oshi/oshi](https://github.com/oshi/oshi)                                     |
| druid              | 1.2.18  | Alibaba 数据库连接池         | [https://github.com/alibaba/druid](https://github.com/alibaba/druid)                             |
| mysql              | 8.0.28  | 关系型数据库，数据持久化           |                                                                                                  |
| mybatis-plus       | 3.4.3.4 | ORM 框架 MyBatis 的增强工具库  | [https://github.com/baomidou/mybatis-plus](https://github.com/baomidou/mybatis-plus)             |
| dynamic-datasource | 3.5.2   | 多数据源、动态数据源             | [https://github.com/baomidou/dynamic-datasource](https://github.com/baomidou/dynamic-datasource) |
| lombok             | 1.18.24 | 通过注解减少样板代码             | [https://github.com/projectlombok/lombok](https://github.com/projectlombok/lombok)               |
| fastjson           | 2.0.39  | JSON 序列化               | [https://github.com/alibaba/fastjson](https://github.com/alibaba/fastjson)                       |
| commons-beanutils  | 1.9.4   | 简化 JavaBean 操作         | [https://github.com/apache/commons-beanutils](https://github.com/apache/commons-beanutils)       |
| commons-lang3      | 3.12.0  | 通用工具类                  |                                                                                                  |
| knife4j            | 3.0.3   | Swagger 前端 UI 增强库      |                                                                                                  |
| spring-boot        | 2.7.15  | 基于 Spring 框架的应用程序的开源框架 |                                                                                                  |
| java               | 1.8     | 开发语言                   |                                                                                                  |

### hello-word-admin

## 分支命名规范

<font color='#F44336'>为了保持代码库的整洁和易于管理，所有团队成员需了解并遵守所约定的分支命名规范。</font>

我们约定的分支命名规则如下：

**开发环境分支名**

- **main**：主分支，用于生产环境存放稳定的、可部署的代码。
- **develop-***：开发分支，用于集成各个开发者的工作。从主分支派生，并且开发团队在这个分支上共同进行开发。

- **feature-***：特性分支，用于开发单一特性或功能。从开发分支派生出来，并在开发完成后合并回开发分支。

- **bugfix-***：修复分支，用于处理 bug 和问题。从开发分支派生出来，并在修复完成后合并回开发分支。

**生产环境分支名**

- **release**：发布分支，用于准备发布到生产环境的代码。从开发分支派生出来，在发布前进行测试和准备工作。

- **hotfix-***：热修复分支，用于紧急修复生产环境中的 bug 或问题。从主分支派生出来，并在修复完成后合并回主分支和开发分支。

## 提交代码规范

**统一的提交规范**有助于团队成员理解提交所做的更改，并且清晰明了。这种规范有助于维护代码库的整洁和易于管理。

所以我们推荐使用以下格式来书写提交信息：

```
feat: *******
```

- **类型(Type)**: 提交的类型，可以是以下之一：
  - feat: 新功能（feature）
  - fix: 修复 bug
  - docs: 文档修改
  - style: 代码格式（不影响代码运行的变动）
  - refactor: 重构（既不修复错误也不添加功能）
  - test: 增加测试
  - chore: 构建过程或辅助工具的变动
- **信息(Message)**: 对本次提交具体进行详细描述，包括解决了什么问题、实现了什么功能、影响了哪些部分等。

## 项目功能点

- [x] 系统登录
- [ ] RBAC 权限控制
- [x] 获取 OS 系统信息
- [x] 获取 Redis 缓存信息
- [x] 获取 Disk 磁盘信息
- [x] 获取 Memory 内存信息
- [x] 获取 JVM 虚拟机信息
- [x] 获取 CPU 处理器信息
- [x] 接口放重复提交
- [ ] 获取用户菜单
- [x] 用户登录日志
- [ ] 操作日志表

## 数据库设计

- [x] 用户表
- [x] 用户组表
- [x] 角色表
- [x] 菜单表
- [x] 用户-用户组关联表
- [x] 用户组-角色关联表
- [x] 角色-菜单关联表
- [x] 登录日志表
- [ ] 操作日志表
