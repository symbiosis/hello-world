/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:ResCodeEnums.java
 *    Date:2023/11/26 下午7:23
 *    Author:赵士杰
 */

package com.hw.exception.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Project hello-world
 * @EnumName ResCodeEnums
 * @Description 响应码枚举类
 * @Author Jie
 * @Date 2023/11/19 11:06
 */
@Getter
@AllArgsConstructor
public enum ResCodeEnums {
    USERNAME_REQUIRED(40001, "用户名必须输入"),
    PASSWORD_REQUIRED(40002, "密码必须输入"),
    USER_NOT_LOGGED_IN(40003, "用户未登录，请登录后操作"),
    INVALID_USERNAME_OR_PASSWORD(40004, "用户名或密码输入错误"),
    USER_BANNED(40005, "该用户已封禁，请联系管理员"),
    USER_KICKED_OUT(40006, "用户被顶下线，请重新登录"),
    USER_LOGGED_OUT(40007, "用户被踢下线，请重新登录"),
    USER_NO_MODIFY_PERMISSION(40008, "当前用户无修改权限"),
    USER_NO_DELETE_PERMISSION(40009, "当前用户无删除权限"),
    USER_NO_ADD_PERMISSION(40010, "当前用户无增加权限"),
    USER_LOGIN_EXPIRED(40011, "用户登录过期，请重新登录"),
    USER_LOGIN_TIMEOUT(40012, "用户登录超时，请重新登录"),
    USER_LOGIN_FAILED(40013, "用户登录失败，请稍后重试");

    private final int code;
    private final String msg;

}
