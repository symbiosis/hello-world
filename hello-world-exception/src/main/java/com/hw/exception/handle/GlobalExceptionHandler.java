/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:GlobalExceptionHandler.java
 *    Date:2023/11/26 下午7:23
 *    Author:赵士杰
 */

package com.hw.exception.handle;

import cn.dev33.satoken.exception.NotLoginException;
import com.hw.common.constant.HttpStatus;
import com.hw.common.entity.AjaxResult;
import com.hw.exception.enums.ResCodeEnums;
import com.hw.exception.exception.CommonException;
import io.netty.channel.ConnectTimeoutException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.nio.channels.ClosedChannelException;

/**
 * @Project hello-world
 * @ClassName GlobalExceptionHandler
 * @Description 全局异常拦截器
 * @Author Jie
 * @Date 2023/10/29 15:21
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(ConnectTimeoutException.class)
    public AjaxResult handleConnectTimeoutException(ConnectTimeoutException e) {
        return AjaxResult.error(HttpStatus.FORBIDDEN, "连接超时，请检查是否正确配置Redis");
    }

    @ExceptionHandler(ClosedChannelException.class)
    public AjaxResult handleClosedChannelException(ClosedChannelException e) {
        return AjaxResult.error(HttpStatus.FORBIDDEN, "连接超时，请检查是否正确配置Redis");
    }

    @ExceptionHandler(CommonException.class)
    public AjaxResult handleCommonException(CommonException e) {
        log.error("错误信息:{}", e.getMessage());
        return AjaxResult.error(e.getCode(), e.getMessage());
    }

    @ExceptionHandler(NotLoginException.class)
    public AjaxResult handlerSaTokenException(NotLoginException e) {
        switch (e.getCode()) {
            case 11001:
            case 11011:
            case 11012:
                return AjaxResult.error(ResCodeEnums.USER_NOT_LOGGED_IN.getCode(), ResCodeEnums.USER_NOT_LOGGED_IN.getMsg());
            case 11013:
                return AjaxResult.error(ResCodeEnums.USER_LOGIN_EXPIRED.getCode(), ResCodeEnums.USER_LOGIN_EXPIRED.getMsg());
            case 11014:
                return AjaxResult.error(ResCodeEnums.USER_KICKED_OUT.getCode(), ResCodeEnums.USER_KICKED_OUT.getMsg());
            case 11015:
                return AjaxResult.error(ResCodeEnums.USER_LOGGED_OUT.getCode(), ResCodeEnums.USER_LOGGED_OUT.getMsg());
            case 11016:
                return AjaxResult.error(ResCodeEnums.USER_LOGIN_TIMEOUT.getCode(), ResCodeEnums.USER_LOGIN_TIMEOUT.getMsg());
            default:
                return AjaxResult.error(ResCodeEnums.USER_LOGIN_FAILED.getCode(), ResCodeEnums.USER_LOGIN_FAILED.getMsg());
        }


    }

}
