/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:CommonException.java
 *    Date:2023/11/26 下午7:23
 *    Author:赵士杰
 */

package com.hw.exception.exception;

import com.hw.exception.enums.ResCodeEnums;
import lombok.Getter;

/**
 * @Project hello-world
 * @ClassName CommonException
 * @Description //TODO
 * @Author Jie
 * @Date 2023/11/1 19:41
 */
@Getter
public class CommonException extends RuntimeException{

    /**
     * 异常细分状态码
     */
    private int code;
    private String message;

    public CommonException() {
        super();
    }

    public CommonException(String message) {
        super(message);
        this.message = message;
    }

    public CommonException(ResCodeEnums code) {
        super();
        this.code = code.getCode();
        this.message = code.getMsg();
    }

    public CommonException(int code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public CommonException(Throwable cause, int code, String message) {
        super(message, cause);
        this.code = code;
        this.message = message;
    }

}
