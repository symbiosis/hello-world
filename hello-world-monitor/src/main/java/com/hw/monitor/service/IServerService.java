/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:IServerService.java
 *    Date:2023/11/18 下午11:45
 *    Author:赵士杰
 */

package com.hw.monitor.service;

import com.hw.monitor.entity.*;

import java.util.List;

/**
 * @Project hello-world
 * @InterfaceName IMonitorService
 * @Description //TODO
 * @Author Jie
 * @Date 2023/11/6 21:30
 */
public interface IServerService {

    /**
     * 获取Redis信息
     * @return Redis
     */
    Redis getRedisInfo();

    /**
     * 获取SystemOs信息
     * @return SystemOs
     */
    SystemOs getSystemOsInfo();

    /**
     * 获取Memory信息
     * @return Memory
     */
    Memory getMemoryInfo();

    /**
     * 获取Jvm信息
     * @return Jvm
     */
    Jvm getJvmInfo();

    /**
     * 获取Disk信息
     * @return Disk
     */
    List<Disk> getDiskInfo();

    /**
     * 获取Cpu信息
     * @return Cpu
     */
    Cpu getCpuInfo();

    /**
     * 获取服务信息
     *
     * @param type
     * @return
     */
    Object getServerInfo(String type);
}
