/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:Redis.java
 *    Date:2023/11/6 下午9:27
 *    Author:赵士杰
 */

package com.hw.monitor.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Project hello-world
 * @ClassName Redis
 * @Description //TODO
 * @Author Jie
 * @Date 2023/11/6 21:27
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Redis {
    /**
     * 操作系统信息
     */
    private String os;
    /**
     * Redis版本
     */
    private String version;
    /**
     * Redis使用的GCC版本
     */
    private String gccVersion;
    /**
     * Redis运行模式
     */
    private String mode;
    /**
     * Redis实例正在侦听的TCP端口号
     */
    private String port;
    /**
     * 当前连接的客户端数量
     */
    private String curConnectedClients;
    /**
     * 历史连接数
     */
    private String hisConnectedClients;
    /**
     * Redis运行时间
     */
    private String runTime;
    /**
     * 内存占用
     */
    private String usedMemory;
    /**
     * Redis内存峰值
     */
    private String usedMemoryPeak;
    /**
     * 存储Lua脚本的内存大小
     */
    private String usedMemoryLua;
    /**
     * 用于存储脚本的内存大小
     */
    private String usedMemoryScripts;
    /**
     * 达到最大内存后的淘汰策略
     */
    private String replacePolicy;
    /**
     * 当前正在保存的键总数
     */
    private String keyTotal;
    /**
     * 键未命中次数
     */
    private String keyMissCount;
    /**
     * 键命中次数
     */
    private String keyHitCount;
    /**
     * 过期键数量
     */
    private String expiredKeyCount;
    /**
     * AOF文件大小
     */
    private String aofSize;
    /**
     * 是否启用AOF持久化，1（已启用）
     */
    private String aofEnabled;
    /**
     * 最后一次AOF写入的状态
     */
    private String aofLastWriteStatus;
    /**
     * 最后一次完成的RDB后台保存的状态，此处为ok。
     */
    private String rdbLastSaveStatus;
    /**
     * 执行RDB保存的次数
     */
    private String rdbSaveCount;
    /**
     * 上次RDB保存的时间
     */
    private String rdbLastSaveTime;
    /**
     * 上次RDB保存以来发生的修改次数，此处为0
     */
    private String rdbChangesSinceLastSave;
    /**
     * 累计网络输出量
     */
    private String totalNetOutputSize;
    /**
     * 累计网络输入字节数
     */
    private String totalNetInputSize;
    /**
     * 历史执行的命令数量
     */
    private String hisCommandProcessed;


}
