/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:Cpu.java
 *    Date:2023/11/5 上午11:27
 *    Author:赵士杰
 */

package com.hw.monitor.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Project hello-world
 * @ClassName Cpu
 * @Description //TODO
 * @Author Jie
 * @Date 2023/11/5 11:27
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Cpu {

    /**
     * CPU名字
     */
    private String name;

    /**
     * CPU物理核心数
     */
    private int physicalCoreNum;

    /**
     * CPU逻辑核心数
     */
    private int logicalCoreNum;

    /**
     * CPU利用率
     */
    private double cpuUsage;

    /**
     * CPU系统使用率
     */
    private double systemUsage;

    /**
     * CPU用户使用率
     */
    private double userUsage;

    /**
     * CPU空闲率
     */
    private double idle;

    /**
     * CPU等待率
     */
    private double wait;

}
