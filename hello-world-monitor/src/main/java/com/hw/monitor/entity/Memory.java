/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:Memory.java
 *    Date:2023/11/5 下午3:59
 *    Author:赵士杰
 */

package com.hw.monitor.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Project hello-world
 * @ClassName Memory
 * @Description 内存
 * @Author Jie
 * @Date 2023/11/5 15:59
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Memory {
    /**
     * 内存总量
     */
    private double total;

    /**
     * 已用内存
     */
    private double used;

    /**
     * 可用内存
     */
    private double available;

    /**
     * 使用率
     */
    private double usage;
}
