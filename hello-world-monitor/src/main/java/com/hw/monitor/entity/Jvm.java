/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:Jvm.java
 *    Date:2023/11/5 下午4:06
 *    Author:赵士杰
 */

package com.hw.monitor.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Project hello-world
 * @ClassName Jvm
 * @Description //TODO
 * @Author Jie
 * @Date 2023/11/5 16:06
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Jvm {

    /**
     * 当前JVM占用的内存总数(M)
     */
    private double total;

    /**
     * JVM最大可用内存总数(M)
     */
    private double max;

    /**
     * JVM已用内存(M)
     */
    private double used;

    /**
     * JVM空闲内存(M)
     */
    private double available;

    /**
     * 资源的使用率
     */
    private double usage;

    /**
     * JDK版本
     */
    private String version;

    /**
     * JDK路径
     */
    private String home;

    /**
     * 项目路径
     */
    private String userDir;

    /**
     * JVM启动时间
     */
    private String startTime;

    /**
     * JVM运行时间
     */
    private String runTime;

    /**
     * 运行参数
     */
    private String inputArgs;

}
