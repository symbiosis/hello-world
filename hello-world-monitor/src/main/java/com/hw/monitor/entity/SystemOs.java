/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:System.java
 *    Date:2023/11/5 下午4:02
 *    Author:赵士杰
 */

package com.hw.monitor.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Project hello-world
 * @ClassName System
 * @Description //TODO
 * @Author Jie
 * @Date 2023/11/5 16:02
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SystemOs {
    /**
     * 服务器名称
     */
    private String computerName;

    /**
     * 服务器Ip
     */
    private String computerIp;

    /**
     * 操作系统
     */
    private String osName;

    /**
     * 系统架构
     */
    private String osArch;
}
