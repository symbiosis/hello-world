/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:Disk.java
 *    Date:2023/11/5 下午4:08
 *    Author:赵士杰
 */

package com.hw.monitor.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Project hello-world
 * @ClassName Disk
 * @Description 磁盘
 * @Author Jie
 * @Date 2023/11/5 16:08
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Disk {
    /**
     * 盘符路径
     */
    private String dirName;

    /**
     * 盘符类型
     */
    private String sysTypeName;

    /**
     * 文件类型
     */
    private String typeName;

    /**
     * 总大小
     */
    private String total;

    /**
     * 剩余大小
     */
    private String available;

    /**
     * 已经使用量
     */
    private String used;

    /**
     * 资源的使用率
     */
    private double usage;
}
