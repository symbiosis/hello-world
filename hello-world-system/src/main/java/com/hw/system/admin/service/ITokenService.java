/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:ITokenService.java
 *    Date:2023/12/3 上午12:03
 *    Author:赵士杰
 */

package com.hw.system.admin.service;

import com.hw.system.admin.vo.TokenConfigVo;

/**
 * @Project hello-world
 * @InterfaceName ITokenService
 * @Description Token 服务接口
 * @Author 赵士杰
 * @Date 2023/11/18 20:27
 */
public interface ITokenService {

    /**
     * 获取 Token 信息
     *
     * @return Token 信息
     */
    TokenConfigVo getTokenInfo();

    /**
     * 更新 Token 配置
     *
     * @param saTokenConfig 新的 Token 配置信息
     */
    void updateTokenConfig(TokenConfigVo tokenConfig);
}
