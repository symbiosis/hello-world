/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:ISysRoleService.java
 *    Date:2023/11/26 下午7:23
 *    Author:赵士杰
 */

package com.hw.system.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hw.system.admin.pojo.SysRole;

import java.util.List;

/**
 * <p>
 * 角色信息表 服务类
 * </p>
 *
 * @author 赵士杰
 * @since 2023-11-26 11:24:43
 */
public interface ISysRoleService extends IService<SysRole> {

    /**
     * 获取用户的角色列表
     *
     * @param userId 用户ID
     * @return 角色列表
     */
    List<SysRole> getUserRole(Long userId);

}
