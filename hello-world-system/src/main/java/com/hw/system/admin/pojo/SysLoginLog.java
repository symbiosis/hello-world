/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:SysLoginLog.java
 *    Date:2023/11/26 下午7:23
 *    Author:赵士杰
 */

package com.hw.system.admin.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * <p>
 * 系统登录记录
 * </p>
 *
 * @author 赵士杰
 * @since 2023-11-26 10:25:50
 */
@Getter
@Setter
@TableName("sys_login_log")
@ApiModel(value = "SysLoginLog对象", description = "系统登录记录")
public class SysLoginLog {

    @ApiModelProperty("访问ID")
    @TableId(value = "log_id", type = IdType.AUTO)
    private Long logId;

    @ApiModelProperty("用户id")
    @TableField("user_id")
    private Long userId;

    @ApiModelProperty("登录IP地址")
    @TableField("ip_addr")
    private String ipAddr;

    @ApiModelProperty("登录省份")
    @TableField("login_province")
    private String loginProvince;

    @ApiModelProperty("登录城市")
    @TableField("login_city")
    private String loginCity;

    @ApiModelProperty("浏览器类型")
    @TableField("browser")
    private String browser;

    @ApiModelProperty("操作系统")
    @TableField("os")
    private String os;

    @ApiModelProperty("登录状态（1成功 0失败）")
    @TableField("status")
    private Boolean status;

    @ApiModelProperty("提示消息")
    @TableField("msg")
    private String msg;

    @ApiModelProperty("访问时间")
    @TableField("login_time")
    private LocalDateTime loginTime;


}
