/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:SysMenuMapper.java
 *    Date:2023/11/26 下午7:23
 *    Author:赵士杰
 */

package com.hw.system.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hw.system.admin.pojo.SysMenu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 菜单权限表 Mapper 接口
 * </p>
 *
 * @author 赵士杰
 * @since 2023-11-26 09:40:05
 */
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    /**
     * 根据用户ID查询用户权限列表
     *
     * @param userId 用户ID
     * @return List<String> 用户权限列表
     */
    List<String> getUserPermissions(Long userId);

    /**
     * 获取所有菜单树
     *
     * @return 所有菜单树的列表
     */
    List<SysMenu> getAllMenuTree();

    /**
     * 根据用户ID获取用户菜单树
     *
     * @param userId 用户ID
     * @return 用户菜单树列表
     */
    List<SysMenu> getUserMenuTree(Long userId);

    /**
     * 根据角色ID获取角色菜单树
     *
     * @param roleId 角色ID
     * @return 角色菜单树列表
     */
    List<SysMenu> getRoleMenuTree(Long roleId);

}
