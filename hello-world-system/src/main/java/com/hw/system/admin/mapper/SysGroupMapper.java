/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:SysGroupMapper.java
 *    Date:2023/12/3 上午12:03
 *    Author:赵士杰
 */

package com.hw.system.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hw.system.admin.pojo.SysGroup;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author 赵士杰
 * @since 2023-11-27 09:07:14
 */
@Mapper
public interface SysGroupMapper extends BaseMapper<SysGroup> {

    /**
     * 查找用户组的回收站
     *
     * @return 用户组回收站列表
     */
    List<SysGroup> findUserGroupRecycle();

    /**
     * 批量恢复被删除的用户组
     *
     * @param groupIds 被删除的用户组ID列表
     * @return 恢复成功返回true，否则返回false
     */
    boolean batchRecoverGroupByIds(List<Integer> groupIds);
}
