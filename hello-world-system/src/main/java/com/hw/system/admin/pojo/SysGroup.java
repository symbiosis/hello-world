/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:SysGroup.java
 *    Date:2023/12/3 上午12:03
 *    Author:赵士杰
 */

package com.hw.system.admin.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author 赵士杰
 * @since 2023-11-27 09:07:14
 */
@Getter
@Setter
@TableName("sys_group")
@ApiModel(value = "SysGroup对象", description = "用户组")
public class SysGroup {

    @ApiModelProperty("用户组id")
    @TableId(value = "group_id", type = IdType.AUTO)
    private Integer groupId;

    @ApiModelProperty("用户组名称")
    @TableField("name")
    private String name;

    @ApiModelProperty("用户组描述")
    @TableField("description")
    private String description;

    @ApiModelProperty("显示顺序")
    @TableField("sort")
    private Integer sort;

    @ApiModelProperty("状态（1正常 0停用）")
    @TableField("status")
    private Boolean status;

    @ApiModelProperty("删除标志（0正常 1停用）")
    @TableLogic(value = "0", delval = "1")
    @TableField("del_flag")
    private Integer delFlag;

    @ApiModelProperty("创建者")
    @TableField("create_by")
    private Long createBy;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty("更新者")
    @TableField("update_by")
    private Long updateBy;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty("更新时间")
    @TableField("update_time")
    private LocalDateTime updateTime;


}
