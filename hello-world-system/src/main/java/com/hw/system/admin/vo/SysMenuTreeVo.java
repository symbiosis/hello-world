/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:SysMenuTreeVo.java
 *    Date:2023/11/26 下午7:23
 *    Author:赵士杰
 */

package com.hw.system.admin.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @Project hello-world
 * @ClassName SysMenuTreeVo
 * @Description 菜单树
 * @Author 赵士杰
 * @Date 2023/11/26 16:58
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class SysMenuTreeVo {

    /**
     * 菜单ID
     */
    private Long menuId;

    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 父菜单ID
     */
    private Long parentId;

    /**
     * 显示顺序
     */
    private Integer sort;

    /**
     * 路由地址
     */
    private String path;

    /**
     * 是否为外链（1是 0否）
     */
    private Boolean isLink;

    /**
     * 菜单类型（M目录 C菜单 F按钮）
     */
    private String menuType;

    /**
     * 菜单状态（1正常 0停用）
     */
    private Boolean status;

    /**
     * 权限标识
     */
    private String permissionSign;

    /**
     * 菜单图标
     */
    private String icon;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新者
     */
    private String updateBy;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 备注
     */
    private String remark;

    /**
     * 子菜单
     */
    private List<SysMenuTreeVo> children;

}

