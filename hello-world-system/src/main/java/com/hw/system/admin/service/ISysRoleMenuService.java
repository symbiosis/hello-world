/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:ISysRoleMenuService.java
 *    Date:2023/11/27 下午9:06
 *    Author:赵士杰
 */

package com.hw.system.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hw.system.admin.pojo.SysRoleMenu;

import java.util.List;

/**
 * <p>
 * 角色和菜单关联表 服务类
 * </p>
 *
 * @author 赵士杰
 * @since 2023-11-27 08:12:41
 */
public interface ISysRoleMenuService extends IService<SysRoleMenu> {

    /**
     * 添加角色和菜单之间的关联关系
     *
     * @param roleId  角色的ID
     * @param menuIds 菜单ID列表
     * @return true 如果成功添加角色菜单关联关系；否则返回false
     */
    boolean addRoleMenu(Long roleId, List<Long> menuIds);

}
