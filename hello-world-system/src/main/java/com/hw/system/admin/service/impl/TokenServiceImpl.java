/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:TokenServiceImpl.java
 *    Date:2023/12/3 上午12:03
 *    Author:赵士杰
 */

package com.hw.system.admin.service.impl;

import cn.dev33.satoken.SaManager;
import cn.dev33.satoken.config.SaTokenConfig;
import com.hw.common.utils.bean.BeanCopyUtils;
import com.hw.common.utils.security.SecurityUtils;
import com.hw.system.admin.service.ITokenService;
import com.hw.system.admin.vo.TokenConfigVo;
import org.springframework.stereotype.Service;

/**
 * @Project hello-world
 * @ClassName TokenServiceImpl
 * @Description //TODO
 * @Author Jie
 * @Date 2023/11/18 20:27
 */
@Service
public class TokenServiceImpl implements ITokenService {

    @Override
    public TokenConfigVo getTokenInfo() {
        SaTokenConfig config = SaManager.getConfig();
        return BeanCopyUtils.copyBean(config, TokenConfigVo.class);
    }

    @Override
    public void updateTokenConfig(TokenConfigVo tokenConfig) {
        SaTokenConfig config = BeanCopyUtils.copyBean(tokenConfig, SaTokenConfig.class);
        SecurityUtils.dynamicSetSaToken(config);
    }

}
