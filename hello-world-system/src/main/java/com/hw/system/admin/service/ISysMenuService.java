/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:ISysMenuService.java
 *    Date:2023/11/26 下午7:23
 *    Author:赵士杰
 */

package com.hw.system.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hw.system.admin.pojo.SysMenu;
import com.hw.system.admin.vo.SysMenuTreeVo;

import java.util.List;

/**
 * <p>
 * 菜单权限表 服务类
 * </p>
 *
 * @author 赵士杰
 * @since 2023-11-26 09:40:05
 */
public interface ISysMenuService extends IService<SysMenu> {

    /**
     * 根据用户ID查询用户权限列表
     *
     * @param userId 用户ID
     * @return List<String> 用户权限列表
     */
    List<String> getUserPermissions(Long userId);

    /**
     * 获取用户菜单树
     *
     * @param userId 用户ID
     * @return 用户菜单树
     */
    List<SysMenuTreeVo> getUserMenuTree(Long userId);

}
