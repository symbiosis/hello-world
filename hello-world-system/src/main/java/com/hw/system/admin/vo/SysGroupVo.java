/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:SysGroupVo.java
 *    Date:2023/12/3 上午12:03
 *    Author:赵士杰
 */

package com.hw.system.admin.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Project hello-world
 * @ClassName SysGroupVo
 * @Description 用户组VO
 * @Author 赵士杰
 * @Date 2023/12/2 21:49
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SysGroupVo {

    /**
     * 用户组id
     */
    private Integer groupId;

    /**
     * 用户组名称
     */
    private String name;

    /**
     * 用户组描述
     */
    private String description;

    /**
     * 显示顺序
     */
    private Integer sort;

    /**
     * 状态（1正常 0停用）
     */
    private Boolean status;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    /**
     * 更新者
     */
    private String updateBy;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;

}
