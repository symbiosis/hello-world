/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:ISysGroupService.java
 *    Date:2023/12/3 上午12:03
 *    Author:赵士杰
 */

package com.hw.system.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hw.system.admin.pojo.SysGroup;
import com.hw.system.admin.vo.SysGroupVo;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author 赵士杰
 * @since 2023-11-27 09:07:14
 */
public interface ISysGroupService extends IService<SysGroup> {

    /**
     * 查找用户组的回收站
     *
     * @return 用户组回收站列表
     */
    List<SysGroupVo> findUserGroupRecycle();

    /**
     * 获取用户组列表
     *
     * @return 用户组列表
     */
    List<SysGroupVo> getGroupList();

    /**
     * 添加用户组
     *
     * @param sysGroup 要添加的用户组对象
     * @return 添加成功返回true，否则返回false
     */
    boolean addGroup(SysGroup sysGroup);

    /**
     * 根据用户组ID更新用户组信息
     *
     * @param sysGroup 要更新的用户组对象
     * @return 更新成功返回true，否则返回false
     */
    boolean updateGroupById(SysGroup sysGroup);

    /**
     * 批量删除用户组
     *
     * @param groupIds 要删除的用户组ID列表
     * @return 删除成功返回true，否则返回false
     */
    boolean batchDeleteGroupByIds(List<Integer> groupIds);

    /**
     * 批量恢复被删除的用户组
     *
     * @param groupIds 被删除的用户组ID列表
     * @return 恢复成功返回true，否则返回false
     */
    boolean batchRecoverGroupByIds(List<Integer> groupIds);

}
