/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:TokenInfoVo.java
 *    Date:2023/12/3 上午12:03
 *    Author:赵士杰
 */

package com.hw.system.admin.vo;

import lombok.Data;

/**
 * @Project hello-world
 * @ClassName TokenInfoVo
 * @Description Token信息
 * @Author 赵士杰
 * @Date 2023/12/2 23:10
 */
@Data
public class TokenInfoVo {

    /**
     * token 名称
     */
    public String tokenName;

    /**
     * token 值
     */
    public String tokenValue;

    /**
     * 此 token 是否已经登录
     */
    public Boolean isLogin;

    /**
     * 登录设备类型
     */
    public String loginDevice;

}
