/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:TokenConfigVo.java
 *    Date:2023/12/3 上午12:03
 *    Author:赵士杰
 */

package com.hw.system.admin.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @Project hello-world
 * @ClassName SaTokenConfigVo
 * @Description SaToken 配置信息实体类
 * @Author Jie
 * @Date 2023/11/18 18:21
 */
@Data
public class TokenConfigVo implements Serializable {
    /**
     * Token 名称 （同时也是 Cookie 名称、数据持久化前缀）
     */
    private String tokenName;

    /**
     * Token 有效期（单位：秒），默认 30 天，-1 代表永不过期
     */
    private long timeout;

    /**
     * Token 最低活跃频率（单位：秒），如果 token 超过此时间没有访问系统就会被冻结，默认-1 代表不限制，永不冻结
     */
    private long activeTimeout;

    /**
     * 是否启用动态 activeTimeout 功能，如不需要请设置为 false，节省缓存请求次数
     */
    private Boolean dynamicActiveTimeout;

    /**
     * 是否允许同一账号并发登录 （为 true 时允许一起登录，为 false 时新登录挤掉旧登录）
     */
    private Boolean isConcurrent;

    /**
     * 在多人登录同一账号时，是否共用一个 token （为 true 时所有登录共用一个 token，为 false 时每次登录新建一个 token）
     */
    private Boolean isShare;

    /**
     * 同一账号最大登录数量，-1代表不限 （只有在 isConcurrent=true，isShare=false 时此配置才有效）
     */
    private int maxLoginCount;

    /**
     * 在每次创建 Token 时的最高循环次数，用于保证 Token 唯一性（-1=不循环重试，直接使用）
     */
    private int maxTryTimes;

    /**
     * 是否读取请求体中的 Token
     */
    private Boolean isReadBody;

    /**
     * 是否读取请求头中的 Token
     */
    private Boolean isReadHeader;

    /**
     * 是否读取 Cookie 中的 Token
     */
    private Boolean isReadCookie;

    /**
     * 是否写入响应头中的 Token
     */
    private Boolean isWriteHeader;

    /**
     * Token 样式（uuid、simple-uuid、random-32、random-64、random-128、tik）
     */
    private String tokenStyle;

    /**
     * 默认数据持久组件实现类中，每次清理过期数据间隔的时间 （单位: 秒） ，默认值30秒，设置为 -1 代表不启动定时清理
     */
    private int dataRefreshPeriod;

    /**
     * 获取 Token-Session 时是否必须登录 （如果配置为true，会在每次获取 Token-Session 时校验是否登录）
     */
    private Boolean tokenSessionCheckLogin;

    /**
     * 是否打开自动续签 （如果此值为true，框架会在每次直接或间接调用 getLoginId() 时进行一次过期检查与续签操作）
     */
    private Boolean autoRenew;

    /**
     * Token 前缀
     */
    private String tokenPrefix;

    /**
     * 是否打印 Sa-Token 执行日志
     */
    private Boolean isPrint;

    /**
     * 是否记录 Sa-Token 日志
     */
    private Boolean isLog;

    /**
     * 日志级别（trace、debug、info、warn、error、fatal）
     */
    private String logLevel;

    /**
     * 日志级别对应的整数值（1=trace、2=debug、3=info、4=warn、5=error、6=fatal）
     */
    private int logLevelInt;

    /**
     * 是否打印彩色日志，true=打印彩色日志，false=打印黑白日志，null=框架根据运行终端自行判断是否打印彩色日志
     */
    private Boolean isColorLog;


}
