/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:SysUserMapper.java
 *    Date:2023/11/19 下午7:03
 *    Author:赵士杰
 */

package com.hw.system.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hw.system.admin.pojo.SysUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 用户信息表 Mapper 接口
 * </p>
 *
 * @author 赵士杰
 * @since 2023-11-19 12:12:26
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

}
