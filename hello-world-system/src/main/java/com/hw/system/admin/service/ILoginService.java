/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:ILoginService.java
 *    Date:2023/12/3 上午12:03
 *    Author:赵士杰
 */

package com.hw.system.admin.service;

import com.hw.system.admin.vo.LoginUserVo;
import com.hw.system.admin.vo.TokenInfoVo;

/**
 * @Project hello-world
 * @InterfaceName ILoginService
 * @Description //TODO
 * @Author 赵士杰
 * @Date 2023/11/18 20:19
 */
public interface ILoginService {

    /**
     * 用户登录
     *
     * @param loginUserVo 登录用户对象
     * @return token
     */
    TokenInfoVo login(LoginUserVo loginUserVo);

}
