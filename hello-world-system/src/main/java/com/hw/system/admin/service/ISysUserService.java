/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:ISysUserService.java
 *    Date:2023/11/19 下午7:03
 *    Author:赵士杰
 */

package com.hw.system.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hw.system.admin.pojo.SysUser;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author 赵士杰
 * @since 2023-11-19 12:12:26
 */
public interface ISysUserService extends IService<SysUser> {

    /**
     * 根据用户名获取用户信息
     *
     * @param username 用户名
     * @return 用户信息
     */
    SysUser getUserByUsername(String username);

    /**
     * 更新用户登录信息
     *
     * @param userId 用户ID
     * @return 更新是否成功
     */
    boolean updateUserLoginInfo(Long userId);

}
