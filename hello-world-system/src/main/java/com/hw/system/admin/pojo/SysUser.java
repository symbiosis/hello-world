/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:SysUser.java
 *    Date:2023/12/3 上午12:03
 *    Author:赵士杰
 */

package com.hw.system.admin.pojo;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * <p>
 * 用户信息表
 * </p>
 *
 * @author 赵士杰
 * @since 2023-11-19 12:12:26
 */
@Data
@TableName("sys_user")
@ApiModel(value = "SysUser对象", description = "用户信息表")
public class SysUser {

    @ApiModelProperty("用户ID")
    @TableId(value = "user_id", type = IdType.AUTO)
    private Long userId;

    @ApiModelProperty("用户昵称")
    @TableField("nickname")
    private String nickname;

    @ApiModelProperty("用户账号")
    @TableField("username")
    private String username;

    @ApiModelProperty("用户密码")
    @TableField("password")
    private String password;

    @ApiModelProperty("用户邮箱")
    @TableField("email")
    private String email;

    @ApiModelProperty("用户手机号码")
    @TableField("phone")
    private String phone;

    @ApiModelProperty("用户性别（0男 1女 2未知）")
    @TableField("sex")
    private String sex;

    @ApiModelProperty("用户头像地址")
    @TableField("avatar")
    private String avatar;

    @ApiModelProperty("用户所在省份")
    @TableField("province")
    private String province;

    @ApiModelProperty("用户所在城市")
    @TableField("city")
    private String city;

    @ApiModelProperty("帐号状态（1正常 0停用）")
    @TableField("status")
    private Boolean status;

    @ApiModelProperty("删除标志（1代表存在 0代表删除）")
    @TableLogic(value = "0", delval = "1")
    @TableField("del_flag")
    private Integer delFlag;

    @ApiModelProperty("最后登录IP")
    @TableField("login_ip")
    private String loginIp;

    @ApiModelProperty("最后登录时间")
    @TableField("login_time")
    private Date loginTime;

    @ApiModelProperty("创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private Date createTime;

    @ApiModelProperty("更新时间")
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}
