/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:SysRoleMenuMapper.java
 *    Date:2023/11/27 下午9:06
 *    Author:赵士杰
 */

package com.hw.system.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hw.system.admin.pojo.SysRoleMenu;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 角色和菜单关联表 Mapper 接口
 * </p>
 *
 * @author 赵士杰
 * @since 2023-11-27 08:12:41
 */
@Mapper
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}
