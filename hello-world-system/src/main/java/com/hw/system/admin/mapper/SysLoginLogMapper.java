/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:SysLoginLogMapper.java
 *    Date:2023/11/19 下午7:03
 *    Author:赵士杰
 */

package com.hw.system.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hw.system.admin.pojo.SysLoginLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 系统访问记录 Mapper 接口
 * </p>
 *
 * @author 赵士杰
 * @since 2023-11-19 12:20:43
 */
@Mapper
public interface SysLoginLogMapper extends BaseMapper<SysLoginLog> {

}
