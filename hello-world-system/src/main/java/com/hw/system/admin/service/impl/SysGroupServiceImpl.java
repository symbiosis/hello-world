/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:SysGroupServiceImpl.java
 *    Date:2023/12/3 上午12:03
 *    Author:赵士杰
 */

package com.hw.system.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hw.common.utils.bean.BeanCopyUtils;
import com.hw.common.utils.security.SecurityUtils;
import com.hw.system.admin.mapper.SysGroupMapper;
import com.hw.system.admin.mapper.SysUserMapper;
import com.hw.system.admin.pojo.SysGroup;
import com.hw.system.admin.pojo.SysUser;
import com.hw.system.admin.service.ISysGroupService;
import com.hw.system.admin.vo.SysGroupVo;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 赵士杰
 * @since 2023-11-27 09:07:14
 */
@Service
public class SysGroupServiceImpl extends ServiceImpl<SysGroupMapper, SysGroup> implements ISysGroupService {

    @Autowired
    private SysGroupMapper sysGroupMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Override
    public List<SysGroupVo> findUserGroupRecycle() {
        // TODO: 2023/12/2 分页 
        List<SysGroup> groupRecycle = sysGroupMapper.findUserGroupRecycle();
        return convertVo(groupRecycle);
    }

    @Override
    public List<SysGroupVo> getGroupList() {
        // TODO: 2023/12/2 分页 
        return convertVo(list());
    }

    @Override
    public boolean addGroup(SysGroup sysGroup) {
        sysGroup.setCreateBy(SecurityUtils.getLoginId());
        sysGroup.setUpdateBy(SecurityUtils.getLoginId());
        return save(sysGroup);
    }

    @Override
    public boolean updateGroupById(SysGroup sysGroup) {
        sysGroup.setUpdateBy(SecurityUtils.getLoginId());
        return updateById(sysGroup);
    }

    @Override
    public boolean batchDeleteGroupByIds(List<Integer> sysGroup) {
        return removeByIds(sysGroup);
    }

    @Override
    public boolean batchRecoverGroupByIds(List<Integer> groupIds) {
        return sysGroupMapper.batchRecoverGroupByIds(groupIds);
    }

    /**
     * 将SysGroup对象列表转换为SysGroupVo对象列表
     *
     * @param groupList 要转换的SysGroup对象列表
     * @return 转换后的SysGroupVo对象列表
     */
    private List<SysGroupVo> convertVo(List<SysGroup> groupList) {
        return groupList.parallelStream()
                .map(i -> {
                    SysGroupVo vo = BeanCopyUtils.copyBean(i, SysGroupVo.class);
                    if (ObjectUtils.isNotEmpty(i.getCreateBy())) {
                        SysUser createUser = sysUserMapper.selectById(i.getCreateBy());
                        vo.setCreateBy(createUser.getNickname());
                    }
                    if (ObjectUtils.isNotEmpty(i.getUpdateBy())) {
                        SysUser updateUser = sysUserMapper.selectById(i.getUpdateBy());
                        vo.setUpdateBy(updateUser.getNickname());
                    }
                    return vo;
                }).collect(Collectors.toList());
    }

}
