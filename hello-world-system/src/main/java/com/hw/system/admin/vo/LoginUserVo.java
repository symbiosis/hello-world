/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:LoginUserVo.java
 *    Date:2023/12/3 上午12:03
 *    Author:赵士杰
 */

package com.hw.system.admin.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Project hello-world
 * @ClassName LoginUserVo
 * @Description //TODO
 * @Author Jie
 * @Date 2023/11/12 21:01
 */
@Data
@ApiModel(value = "LoginUserVo对象", description = "用户登录信息表")
public class LoginUserVo {

    @ApiModelProperty("用户账号")
    private String username;

    @ApiModelProperty("用户密码")
    private String password;

}
