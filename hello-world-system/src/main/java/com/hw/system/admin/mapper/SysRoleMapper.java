/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:SysRoleMapper.java
 *    Date:2023/11/26 下午7:23
 *    Author:赵士杰
 */

package com.hw.system.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hw.system.admin.pojo.SysRole;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 角色信息表 Mapper 接口
 * </p>
 *
 * @author 赵士杰
 * @since 2023-11-26 11:24:43
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {

    /**
     * 获取用户的角色列表
     *
     * @param userId 用户ID
     * @return 角色列表
     */
    List<SysRole> getUserRole(Long userId);
}
