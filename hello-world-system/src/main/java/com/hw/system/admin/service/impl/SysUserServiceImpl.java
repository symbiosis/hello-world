/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:SysUserServiceImpl.java
 *    Date:2023/11/19 下午7:03
 *    Author:赵士杰
 */

package com.hw.system.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hw.common.utils.ip.IpUtils;
import com.hw.system.admin.mapper.SysUserMapper;
import com.hw.system.admin.pojo.SysUser;
import com.hw.system.admin.service.ISysUserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户信息表 服务实现类
 * </p>
 *
 * @author 赵士杰
 * @since 2023-11-19 12:12:26
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {

    @Override
    public SysUser getUserByUsername(String username) {
        LambdaQueryWrapper<SysUser> userLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userLambdaQueryWrapper.eq(SysUser::getUsername, username);
        return getOne(userLambdaQueryWrapper);
    }

    @Override
    public boolean updateUserLoginInfo(Long userId) {
        SysUser user = new SysUser();
        user.setUserId(userId);
        String ipAddr = IpUtils.getIpAddr();
        user.setLoginIp(ipAddr);
        return updateById(user);
    }

}
