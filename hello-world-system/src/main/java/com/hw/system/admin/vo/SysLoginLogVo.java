/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:SysLoginLogVo.java
 *    Date:2023/11/19 下午7:03
 *    Author:赵士杰
 */

package com.hw.system.admin.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @Project hello-world
 * @ClassName SysLoginLogVo
 * @Description //TODO
 * @Author 赵士杰
 * @Date 2023/11/19 17:59
 */
@Data
public class SysLoginLogVo {
    /**
     * 日志ID
     */
    private Long logId;

    /**
     * 用户名
     */
    private String username;

    /**
     * 登录IP地址
     */
    private String ipaddr;

    /**
     * 登录省份
     */
    private String loginProvince;

    /**
     * 登录城市
     */
    private String loginCity;

    /**
     * 浏览器类型
     */
    private String browser;

    /**
     * 操作系统
     */
    private String os;

    /**
     * 登录状态（1成功 0失败）
     */
    private Boolean status;

    /**
     * 提示消息
     */
    private String msg;

    /**
     * 登录时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date loginTime;

}
