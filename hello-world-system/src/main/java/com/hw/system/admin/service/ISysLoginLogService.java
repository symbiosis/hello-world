/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:ISysLoginLogService.java
 *    Date:2023/11/19 下午7:03
 *    Author:赵士杰
 */

package com.hw.system.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hw.system.admin.pojo.SysLoginLog;
import com.hw.system.admin.vo.SysLoginLogVo;

import java.util.List;

/**
 * <p>
 * 系统访问记录 服务类
 * </p>
 *
 * @author 赵士杰
 * @since 2023-11-19 12:20:43
 */
public interface ISysLoginLogService extends IService<SysLoginLog> {

    /**
     * 记录用户登录信息
     *
     * @param userId 用户id
     * @param status 登录是否成功
     * @param msg    登录成功信息
     * @return 记录是否成功
     */
    boolean recordLoginInfo(Long userId, boolean status, String msg);

    /**
     * 获取登录日志列表
     *
     * @return 登录日志列表
     */
    List<SysLoginLogVo> getLoginLogList();

}
