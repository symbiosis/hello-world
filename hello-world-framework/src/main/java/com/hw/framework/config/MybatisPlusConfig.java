/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:MybatisPlusConfig.java
 *    Date:2023/11/19 下午7:03
 *    Author:赵士杰
 */

package com.hw.framework.config;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Project hello-world
 * @ClassName MybatisPlusConfig
 * @Description MybatisPlus 配置类
 * @Author 赵士杰
 * @Date 2023/11/19 17:51
 */
@Configuration
public class MybatisPlusConfig {

    /**
     * 配置Mybatis-Plus的分页插件
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor mybatisPlusInterceptor = new MybatisPlusInterceptor();
        mybatisPlusInterceptor.addInnerInterceptor(new PaginationInnerInterceptor());
        return mybatisPlusInterceptor;
    }

}
