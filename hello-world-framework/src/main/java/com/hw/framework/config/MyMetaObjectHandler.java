/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:MyMetaObjectHandler.java
 *    Date:2023/11/19 下午7:03
 *    Author:赵士杰
 */

package com.hw.framework.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @Project hello-world
 * @ClassName MyMetaObjectHandler
 * @Description MyBatisPlus 字段自动填充
 * @Author 赵士杰
 * @Date 2023/11/19 17:47
 */
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        this.setFieldValByName("createTime", new Date(), metaObject);
        this.setFieldValByName("updateTime", new Date(), metaObject);
        this.setFieldValByName("loginTime", new Date(), metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.setFieldValByName("updateTime", new Date(), metaObject);
    }

}
