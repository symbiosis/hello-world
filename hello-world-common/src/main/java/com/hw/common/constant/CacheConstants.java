/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:CacheConstants.java
 *    Date:2023/10/29 下午8:01
 *    Author:赵士杰
 */

package com.hw.common.constant;

/**
 * @Project：hello-world
 * @ClassName：CacheConstants
 * @Description：//TODO
 * @Author：Jie
 * @Date：2023/10/24 22:13
 */
public class CacheConstants {
    /**
     * 登录用户 redis key
     */
    public static final String LOGIN_TOKEN_KEY = "login_tokens:";

    /**
     * 验证码 redis key
     */
    public static final String CAPTCHA_CODE_KEY = "captcha_codes:";

    /**
     * 参数管理 cache key
     */
    public static final String SYS_CONFIG_KEY = "sys_config:";

    /**
     * 字典管理 cache key
     */
    public static final String SYS_DICT_KEY = "sys_dict:";

    /**
     * 防重提交 redis key
     */
    public static final String REPEAT_SUBMIT_KEY = "repeat_submit:";

    /**
     * 限流 redis key
     */
    public static final String RATE_LIMIT_KEY = "rate_limit:";

    /**
     * 登录账户密码错误次数 redis key
     */
    public static final String PWD_ERR_CNT_KEY = "pwd_err_cnt:";
}
