/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:ServerConstants.java
 *    Date:2023/11/18 下午11:45
 *    Author:赵士杰
 */

package com.hw.common.constant;

/**
 * @Project hello-world
 * @ClassName ServerConstants
 * @Description 服务类型
 * @Author Jie
 * @Date 2023/11/18 21:32
 */
public class ServerConstants {
    public static final String REDIS = "redis";
    public static final String CPU = "cpu";
    public static final String OS = "os";
    public static final String DISK = "disk";
    public static final String JVM = "jvm";
    public static final String MEMORY = "memory";
}
