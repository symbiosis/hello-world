/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:SecurityUtils.java
 *    Date:2023/11/26 下午7:23
 *    Author:赵士杰
 */

package com.hw.common.utils.security;

import cn.dev33.satoken.SaManager;
import cn.dev33.satoken.config.SaTokenConfig;
import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;

/**
 * @Project hello-world
 * @ClassName SecurityUtils
 * @Description //TODO
 * @Author Jie
 * @Date 2023/11/18 17:39
 */
@SuppressWarnings("all")
public class SecurityUtils {

    /**
     * 在服务运行时动态设置 Sa-Token
     */
    public static void dynamicSetSaToken(SaTokenConfig config) {
        // 通过 SaManager 对象动态设置 Sa-Token 配置
        SaManager.setConfig(config);
    }

    /**
     * 判断当前登录用户是否是管理员角色
     *
     * @return true表示当前登录用户是管理员角色，false表示当前登录用户不是管理员角色
     */
    public static boolean isAdmin(Long id) {
        return StpUtil.hasRole(id, "admin");
    }

    /**
     * 获取登录用户的ID
     *
     * @return 登录用户的ID，如果用户未登录则返回null
     */
    public static Long getLoginId() {
        return StpUtil.getLoginIdAsLong();
    }

    /**
     * 用户登录并返回Token信息
     *
     * @param userId 用户ID
     * @param device 设备信息
     * @return SaTokenInfo 对象，包含用户的Token信息
     */
    public static SaTokenInfo login(Long userId, String device) {
        // 用户登录
        StpUtil.login(userId, device);
        // 返回用户的Token信息
        return StpUtil.getTokenInfo();
    }


    /**
     * 用户登录并返回Token信息
     *
     * @param userId 用户ID
     * @return SaTokenInfo 对象，包含用户的Token信息
     */
    public static SaTokenInfo login(Long userId) {
        // 用户使用默认设备登录
        StpUtil.login(userId);
        // 返回用户的Token信息
        return StpUtil.getTokenInfo();
    }


    /**
     * 验证 Token 是否登录
     *
     * @param token Token 字符串
     * @return true - 有效；false - 无效
     */
    public static boolean isLogin(String token) {
        return StpUtil.isLogin(token);
    }

    /**
     * 获取 Token 关联的用户ID
     *
     * @param token Token 字符串
     * @return 用户ID
     */
    public static Object getUserIdFromToken(String token) {
        return StpUtil.getLoginIdByToken(token);
    }

}
