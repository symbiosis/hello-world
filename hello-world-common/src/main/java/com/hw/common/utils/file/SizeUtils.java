/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:SizeUtils.java
 *    Date:2023/11/5 下午8:00
 *    Author:赵士杰
 */

package com.hw.common.utils.file;

/**
 * @Project hello-world
 * @ClassName SizeUtils
 * @Description //TODO
 * @Author Jie
 * @Date 2023/11/5 20:00
 */
public class SizeUtils {

    /**
     * 字节大小转换
     *
     * @param size 字节大小
     * @return 转换后值
     */
    public static String byteConvert(long size) {
        long kb = 1024;
        long mb = kb << 10;
        long gb = mb << 10;
        if (size >= gb)
        {
            return String.format("%.1f GB", (float) size / gb);
        }
        else if (size >= mb)
        {
            float f = (float) size / mb;
            return String.format(f > 100 ? "%.0f MB" : "%.1f MB", f);
        }
        else if (size >= kb)
        {
            float f = (float) size / kb;
            return String.format(f > 100 ? "%.0f KB" : "%.1f KB", f);
        }
        else
        {
            return String.format("%d B", size);
        }
    }

}
