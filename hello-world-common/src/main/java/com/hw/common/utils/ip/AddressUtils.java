/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:AddressUtils.java
 *    Date:2023/11/18 下午11:45
 *    Author:赵士杰
 */

package com.hw.common.utils.ip;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.hw.common.constant.Constants;
import com.hw.common.utils.http.HttpUtils;
import com.hw.common.utils.string.StringUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * @Project hello-world
 * @ClassName AddressUtils
 * @Description 获取地址
 * @Author Jie
 * @Date 2023/10/21 20:06
 */
@Slf4j
public class AddressUtils {

    // IP地址查询
    public static final String IP_URL = "http://whois.pconline.com.cn/ipJson.jsp";

    // 未知地址
    public static final String UNKNOWN = "XX XX";
    public static final String INTERNAL_IP = "内网IP";

    public static String getRealAddressByIP(String ip)
    {
        // 内网不查询
        if (IpUtils.internalIp(ip))
        {
            return INTERNAL_IP;
        }

            try
            {
                String rspStr = HttpUtils.sendGet(IP_URL, "ip=" + ip + "&json=true", Constants.GBK);
                if (StringUtils.isEmpty(rspStr))
                {
                    log.error("获取地理位置异常 {}", ip);
                    return UNKNOWN;
                }
                JSONObject obj = JSON.parseObject(rspStr);
                String region = obj.getString("pro");
                String city = obj.getString("city");
                return String.format("%s %s", region, city);
            }
            catch (Exception e)
            {
                log.error("获取地理位置异常 {}", ip);
            }

        return UNKNOWN;
    }

}
