/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:UserAgentUtils.java
 *    Date:2023/11/18 下午11:45
 *    Author:赵士杰
 */

package com.hw.common.utils.ip;

import com.blueconic.browscap.*;
import com.hw.common.enums.UserAgentEnums;
import com.hw.common.utils.servlet.ServletUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;

/**
 * @Project hello-world
 * @ClassName UserAgentUtils
 * @Description 用户代理工具类
 * @Author Jie
 * @Date 2023/11/17 22:22
 */
public class UserAgentUtils {

    private static final UserAgentParser userAgentParser;

    static {
        try {
            userAgentParser = new UserAgentService().loadParser(Arrays.asList(
                    BrowsCapField.BROWSER, BrowsCapField.BROWSER_TYPE,
                    BrowsCapField.BROWSER_MAJOR_VERSION, BrowsCapField.DEVICE_TYPE,
                    BrowsCapField.PLATFORM, BrowsCapField.PLATFORM_VERSION,
                    BrowsCapField.RENDERING_ENGINE_VERSION, BrowsCapField.RENDERING_ENGINE_NAME,
                    BrowsCapField.PLATFORM_MAKER, BrowsCapField.RENDERING_ENGINE_MAKER
            ));
        } catch (IOException | ParseException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 获取设备类型
     *
     * @return 设备类型
     */
    public static String getDeviceType() {
        return getUserTerminal(ServletUtils.getRequest(), UserAgentEnums.DEVICETYPE);
    }

    /**
     * 获取浏览器
     *
     * @return 浏览器
     */
    public static String getBrowser() {
        return getUserTerminal(ServletUtils.getRequest(), UserAgentEnums.BROWSER);
    }

    /**
     * 获取浏览器类型
     *
     * @return 浏览器类型
     */
    public static String getBrowserType() {
        return getUserTerminal(ServletUtils.getRequest(), UserAgentEnums.BROWSERTYPE);
    }

    /**
     * 获取浏览器主版本号
     *
     * @return 浏览器主版本号
     */
    public static String getBrowserMajorVersion() {
        return getUserTerminal(ServletUtils.getRequest(), UserAgentEnums.BROWSERMAJORVERSION);
    }

    /**
     * 获取平台
     *
     * @return 平台
     */
    public static String getPlatform() {
        return getUserTerminal(ServletUtils.getRequest(), UserAgentEnums.PLATFORM);
    }

    /**
     * 根据指定的用户代理信息枚举类型，从HttpServletRequest中获取用户代理信息并返回相应的信息
     *
     * @param request HTTP请求对象
     * @param info    用户代理信息枚举类型
     * @return 用户代理信息对应的值
     */
    public static String getUserTerminal(HttpServletRequest request, UserAgentEnums info) {
        // 获取用户代理信息
        String userAgent = request.getHeader("User-Agent");
        // 解析用户代理信息
        Capabilities capabilities = userAgentParser.parse(userAgent);
        // 获取设备信息
        switch (info) {
            case BROWSER:
                return capabilities.getBrowser();
            case BROWSERTYPE:
                return capabilities.getBrowserType();
            case BROWSERMAJORVERSION:
                return capabilities.getBrowserMajorVersion();
            case PLATFORM:
                return capabilities.getPlatform();
            default:
                return capabilities.getDeviceType();
        }
    }

}
