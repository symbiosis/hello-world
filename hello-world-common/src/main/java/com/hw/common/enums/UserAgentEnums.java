/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:UserAgentEnums.java
 *    Date:2023/11/18 下午11:45
 *    Author:赵士杰
 */

package com.hw.common.enums;

import lombok.Getter;

/**
 * @Project hello-world
 * @EnumName UserAgentEnums
 * @Description //TODO
 * @Author Jie
 * @Date 2023/11/18 20:52
 */
@Getter
@SuppressWarnings("all")
public enum UserAgentEnums {
    BROWSER("Browser"),
    BROWSERTYPE("BrowserType"),
    BROWSERMAJORVERSION("BrowserMajorVersion"),
    PLATFORM("Platform"),
    DEVICETYPE("DeviceType");

    private final String info;

    UserAgentEnums(String info) {
        this.info = info;
    }

}
