/*
 *    Copyright(c)2023-2024
 *    项目名称:hello-world
 *    文件名称:RepeatSubmit.java
 *    Date:2023/10/29 下午8:01
 *    Author:赵士杰
 */

package com.hw.common.annotation;

import java.lang.annotation.*;

/**
 * @Project：hello-world
 * @Annotation：RepeatSubmit
 * @Description：重复提交
 * @Author：Jie
 * @Date：2023/10/24 23:25
 */
@Inherited
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RepeatSubmit {

    /**
     * 间隔时间(ms)，小于此时间视为重复提交
     */
    public int interval() default 50000;

    /**
     * 提示消息
     */
    public String message() default "不允许重复提交，请稍候再试";

}
